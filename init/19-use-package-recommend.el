;; http://emacs.rubikitch.com/tag/recommended-packages/

;; http://emacs.rubikitch.com/iflipb/
(use-package iflipb
  :bind (("M-o" . 'iflipb-next-buffer)
	 ("M-O" . 'iflipb-previous-buffer))
  )

;; http://emacs.rubikitch.com/which-key/
(use-package which-key
  :init
  ;; 3つの表示方法どれか1つ選ぶ
  (which-key-setup-side-window-bottom)    ;ミニバッファ
  ;; (which-key-setup-side-window-right)     ;右端
  ;; (which-key-setup-side-window-right-bottom) ;両方使う
  (which-key-mode 1)
  )

;;(use-package init-open-recentf
;;  :init
;;  (recentf-mode 1)
;;  ;; ido/anything/helmのうちどれかを指定する
;;  (setq init-open-recentf-interface 'ido)
;;  (init-open-recentf)
;;  )

(use-package dashboard
  :config
  (setq dashboard-startup-banner
	(concat data-directory "../../../icons/hicolor/128x128/apps/emacs.png");; もう少し大きくしたい
	;;(concat data-directory "../../../icons/hicolor/scalable/apps/emacs.svg") ;; 表示が小さい。大きく表示したい
	)
  (dashboard-setup-startup-hook)
  (setq dashboard-items '((recents  . 5)
                          (bookmarks . 5)
                          (projects . 5)
                          (agenda . 5)
                          (registers . 5)))
  )

(use-package minimap
  :commands
  (minimap-bufname minimap-create minimap-kill)
  :custom
  (minimap-major-modes '(prog-mode))

  (minimap-window-location 'right)
  (minimap-update-delay 0.2)
  (minimap-minimum-width 20)
  :bind
  (("C-c m" . ladicle/toggle-minimap))
  :preface
  (defun ladicle/toggle-minimap ()
    "Toggle minimap for current buffer."
    (interactive)
    (if (null minimap-bufname)
	(minimap-create)
      (minimap-kill)))
  :config
  (custom-set-faces
   '(minimap-active-region-background
     ((((background dark)) (:background "#555555555555"))
      (t (:background "#C847D8FEFFFF"))) :group 'minimap)))
