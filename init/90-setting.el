;; custom-file 
(setq custom-file
      (expand-file-name "custom.el" user-emacs-directory))

(when (file-exists-p custom-file)
  (load custom-file))


;; c c++ mode
(defun my-c-c++-mode-init ()
  (setq c-basic-offset 2))

(add-hook 'c-mode-hook 'my-c-c++-mode-init)
(add-hook 'c++-mode-hook 'my-c-c++-mode-init)

;; mode line
(display-time-mode 1)
(line-number-mode 1)

;; 矢印キーでバッファ移動
(global-set-key (kbd "C-c <left>")  'windmove-left)
(global-set-key (kbd "C-c <right>") 'windmove-right)
(global-set-key (kbd "C-c <up>")    'windmove-up)
(global-set-key (kbd "C-c <down>")  'windmove-down)
(setq windmove-wrap-around t);; 端では反対側に移動

;; font
(set-face-attribute 'default nil
		    :family "Migu 1M" :height 200)
(set-fontset-font nil 'japanese-jisx0208
		  (font-spec :family "Migu 1M"))


;; yes/no の選択を y/n に
(defalias 'yes-or-no-p 'y-or-n-p)


;; key
(setq mac-option-modifier 'meta)
(setq mac-command-modifier 'meta)

;; clipboard 
(set-clipboard-coding-system 'sjis-mac)




;;
(setq mac-pass-command-to-system nil)
(setq mac-pass-control-to-system nil)






;; フルスクリーンに 
;;(toggle-frame-fullscreen)

