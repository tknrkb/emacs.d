;; skk
(use-package ddskk
  :bind (("C-x j" . skk-mode)
	 ("C-x C-j" . skk-mode))
  :init (progn 
	  (setq skk-server-host "localhost"
		skk-server-portnum 1178)
	  (require 'ccc)

	  (message "skk init!")
	  )
  ;; パッケージ名が autoload に指定出来る形式でないため autoload が失敗するため
  ;; :configは実行されない
  )


;; magit
(use-package magit
  :bind (("C-x g" . 'magit-status)))


;; projectile
(use-package projectile
;;  :bind (("C-p s" . projectile-switch-open-project)
;;	 ("C-x p" . projectile-switch-project))
  :config
  (define-key projectile-mode-map (kbd "s-p") 'projectile-command-map)
  (define-key projectile-mode-map (kbd "C-c p") 'projectile-command-map)
  (projectile-mode +1)

  (projectile-global-mode)
  (setq projectile-enable-caching t)
  )

;; neotree
(use-package neotree
  )


;; ido
(use-package ido
  :bind (("C-x C-f" . ido-find-file))
  :config
  (ido-mode t)
  (ido-everywhere t)
  (setq ido-enable-flex-matching t)
  )

(use-package flx-ido
  :config
  (flx-ido-mode 1)
  (setq flx-ido-use-faces nil
        flx-ido-threshold 10000)
  )

(use-package smex
  :bind (("M-x" . smex))
  :config
  (setq smex-auto-update t
        ;smex-save-file (concat my:d:tmp "smex-items")
        smex-prompt-string "smex: "
        smex-flex-matching nil
        )
  )

(use-package ido-vertical-mode
  :init
  (ido-mode 1)
  (ido-vertical-mode 1)
  (setq ido-vertical-define-keys 'C-n-C-p-up-down-left-right)
  )


;; yatemplate
(use-package s)

(use-package yasnippet-snippets)

(use-package yasnippet
  :diminish yas-minor-mode
  :bind (:map yas-minor-mode-map
              ("C-x i i" . yas-insert-snippet)
              ("C-x i n" . yas-new-snippet)
              ("C-x i v" . yas-visit-snippet-file)
              ("C-x i l" . yas-describe-tables)
              ("C-x i g" . yas-reload-all)
              )
  :config
  (yas-global-mode 1)
  (setq yas-prompt-functions '(yas-ido-prompt))
  (setq yas-snippet-dirs
	'("~/.emacs.d/snippets"
	  "~/.emacs.d/straight/build/yasnippet-snippets/snippets"))
  )


(use-package yatemplate
  :init
  (yatemplate-fill-alist)
  (auto-insert-mode 1)
  (defun find-file-hook--yatemplate ()
    "yatemplateのsnippetのテストができるようにするためにsnippet-modeにする"
    (when (string-match "emacs.*/templates/" buffer-file-name)
      (let ((mode major-mode))
	(snippet-mode)
	(setq-local yas--guessed-modes (list mode)))))
  (add-hook 'find-file-hook 'find-file-hook--yatemplate)
  (defun after-save-hook--yatemplate ()
    "yatemplateファイル保存後、auto-insert-alistに反映させる"
    (when (string-match "emacs.*/templates/" buffer-file-name)
      (yatemplate-fill-alist)))
  (add-hook 'after-save-hook 'after-save-hook--yatemplate)
  )
