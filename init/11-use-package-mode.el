;; web-mode
(use-package web-mode)

;; vue-mode
(use-package vue-mode)



;; editconfig
(use-package editorconfig
  :init
  (editorconfig-mode 1)

  :config
  (add-to-list 'editorconfig-indentation-alist
               '(vue-mode css-indent-offset
                          js-indent-level
                          sgml-basic-offset
                          ssass-tab-width))
  )
